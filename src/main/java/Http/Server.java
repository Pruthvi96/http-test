package Http;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.PathMatchers;
import akka.http.javadsl.server.Route;
import akka.pattern.Patterns;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;
import Http.GreetingMessages.*;

import java.time.Duration;
import java.util.concurrent.CompletionStage;

public class Server extends AllDirectives {
    private static final String RESOURCE_PATH = "C:\\Users\\Nuiok\\Desktop\\Akkahttp\\App\\src\\main\\resources\\pages";

    public static void main(String[] args) throws Exception {
        ActorSystem system = ActorSystem.create("helloAkkaHttpServer");

        final Http http = Http.get(system);
        final ActorMaterializer materializer = ActorMaterializer.create(system);

        Server app = new Server(system);

        final Flow<HttpRequest, HttpResponse, NotUsed> routeFlow = app.createRoute().flow(system, materializer);
        http.bindAndHandle(routeFlow, ConnectHttp.toHost("localhost", 9191), materializer);

        System.out.println("Server online at http://localhost:9191");
    }

    private ActorRef testActor;
    private Duration timeout = Duration.ofSeconds(5l);

    public Server(ActorSystem system) { testActor = system.actorOf(TestActor.props());
    }

    /**
     * Here you can define all the different routes you want to have served by this web server
     * Note that routes might be defined in separated classes like the current case
     */
    protected Route createRoute() {
        return route(
                pathEnd(() -> {
                    return complete("Hi");
                }),
                pathPrefix("api", () ->
                        getActorRoutes()
                ),
                pathPrefix("pages", () ->
                        route (
                                pathEnd(() ->
                                        loadFile("index.html")),
                                route(path(PathMatchers.segment(), name ->
                                        loadFile(name)
                                )))));
    }

    private Route loadFile(String name) {
        return getFromFile(RESOURCE_PATH + (name=="" ? "index.html":name));
    }

    private Route getActorRoutes() {
        return route(
                pathPrefix("greeting", () ->
                        getGreeterRoutes()
                )
        );
    }

    private Route getGreeterRoutes() {
        return route(
                get(() -> {
                    CompletionStage<Greeting> greeting = Patterns
                            .ask(testActor, new RequestGreeting(), timeout)
                            .thenApply(Greeting.class::cast);

                    return onSuccess(() -> greeting,
                            msg ->
                                    complete(StatusCodes.OK, msg, Jackson.marshaller())
                    );
                })
        );
    }
}