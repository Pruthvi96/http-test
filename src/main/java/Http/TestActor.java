package Http;

import akka.actor.AbstractActor;
import akka.actor.Props;

class TestActor extends AbstractActor {
    public static Props props() { return Props.create(TestActor.class);
    }

    public Receive createReceive() {
        return receiveBuilder()
                .match(GreetingMessages.RequestGreeting.class, msg ->
                        getSender().tell(new GreetingMessages.Greeting("Test1"), getSelf()))
                .build();
    }
}