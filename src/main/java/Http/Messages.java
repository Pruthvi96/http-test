package Http;

import java.io.Serializable;

interface GreetingMessages {
    class RequestGreeting implements Serializable {}

    class Greeting implements Serializable {
        public String message;
        public Greeting(String message) {this.message=message;}

    }
}
